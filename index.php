<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <script src="https://code.jquery.com/jquery-2.2.4.min.js" integrity="sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44=" crossorigin="anonymous"></script>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-KyZXEAg3QhqLMpG8r+8fhAXLRk2vvoC2f3B09zVXn8CA5QIVfZOJ3BCsw2P0p/We" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-U1DAWAznBHeqEIlVSCgzq+c9gqGAJn5c/t99JyeKa9xxaYpSvHU5awsuZVVFIhvj" crossorigin="anonymous"></script>
</head>
<body>
    <?php
        $url = 'https://newsapi.org/v2/everything?q=tesla&from=2021-08-23&sortBy=publishedAt&apiKey=ad5e62a1f23940eab166171971368a2c';
        $rs = file_get_contents($url);
        $datos = json_decode($rs, true);
        // echo var_dump($datos['articles']);
        $notas = count($datos['articles']);
        // echo $notas;
        $por_pag = 10;
        $pag = $notas/$por_pag;
        $pag = ceil($pag);
    ?>

    <div class="container my-5">
        <?php 
            if(!$_GET){
                header('Location:index.php?pagina=1');
            }
            
            
            if($_GET['pagina']-1){
                $salida = array_slice($datos['articles'], 0, 10);
            }else{
                $salida = array_slice($datos['articles'], 11, 19);
            }
            
            // echo $salida;
        ?>
        
        <?php //foreach($datos['articles'] as $key => $d): ?>
        <?php foreach($salida as $key => $d): ?>
        <div class="row alert alert-primary">
            <div class="col-md-4 ">
                <img src="<?php echo $d['urlToImage'] ?>" alt="" width="100%">
            </div>
            <div class="col-md-8 ">
                <p class="autor">Autor: <span id="autor-<?= $key ?>"></span></p>
                <p>Titulo: <?php echo $d['title'] ?></p>
                <p>Descripción: <?php echo $d['description'] ?></p>
                <p>Contenido: <?php echo $d['content'] ?></p>
            </div>
        </div>
        <?php endforeach ?>
    </div>
    <nav aria-label="Page navigation example">
        <ul class="pagination justify-content-center">
            <li class="page-item <?php echo $_GET['pagina']<=1 ? 'disabled':'' ?>"><a class="page-link" href="index.php?pagina=<?php echo $_GET['pagina']-1 ?>" >Anterior</a></li>
            <?php for($i=0; $i<$pag; $i++): ?>
            <li class="page-item <?php echo $_GET['pagina']==$i+1 ? 'active':'' ?>"><a class="page-link" href="index.php?pagina=<?php echo $i+1 ?>"> <?php echo $i+1 ?> </a></li>
            <?php endfor?>
            <li class="page-item <?php echo $_GET['pagina']>=$pag ? 'disabled':'' ?>"><a class="page-link" href="index.php?pagina=<?php echo $_GET['pagina']+1 ?>">Siguiente</a></li>
        </ul>
    </nav>
    <script>
        $(document).ready(function(){
            var autor = '';
            var aut = '';
            fetch("https://randomuser.me/api/?results=<?= count($datos['articles'])?>")
            .then((res) => res.json())
            .then((data) => {
                console.log(data)
                data.results.map((user, index)=>{
                    const autor = $(`#autor-${index}`);
                    autor.text(`${user.name.first} ${user.name.last}`);
                    console.log(autor);
                })
            })
        })
    </script>
</body>
</html>


